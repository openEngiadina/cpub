# CPub v0.3.0: ActivityPub Client-to-Server Protocol

<!-- These release notes were initially posted to the SocialHub forum (https://socialhub.activitypub.rocks/t/cpub-v0-3-0-activitypub-client-to-server-protocol/2393). -->

It is my great pleasure to announce a release of CPub - an experimental generic ActivityPub server that uses Semantic Web ideas.

This release ([v0.3.0](https://codeberg.org/openEngiadina/cpub/releases/tag/v0.3.0)) adds initial support of the [ActivityPub Client-to-Server (C2S) Protocol](https://www.w3.org/TR/activitypub/#client-to-server-interactions) by implementing:

- [JSON-LD](https://json-ld.org/) serialization (previously CPub only used [RDF/Turtle](https://www.w3.org/TR/turtle/) and [RDF/JSON](https://www.w3.org/TR/rdf-json/))
- [NodeInfo](https://github.com/jhass/nodeinfo/blob/main/PROTOCOL.md)
- [WebFinger](https://datatracker.ietf.org/doc/html/rfc7033)
- The pump.io `/api/whoami` endpoint

ActivityPub C2S was developed in order to support clients such as AndStatus (see [this AndStatus issue](https://github.com/andstatus/andstatus/issues/499)).

In addition this release also includes following changes:

- Use of the Erlang/OTP in-built [mnesia](https://www.erlang.org/doc/man/mnesia.html) database. This allows easier deployment as no additional database server (e.g. PostgresSQL) needs to be setup.
- Initial support for dereferencing [Magnet URIs](https://en.wikipedia.org/wiki/Magnet_URI_scheme) (see [this thread](https://socialhub.activitypub.rocks/t/content-addressing-and-urn-resolution/1674)).

The source code of CPub is available at [codeberg](https://codeberg.org/openengiadina/cpub) (AGPL-3.0-or-later). Some documentation is available online [here](https://openengiadina.codeberg.page/cpub) (including a [small demo](https://openengiadina.codeberg.page/cpub/demo.html#content)).

This is the final release of CPub that will be made as part of the [openEngiadina](https://openengiadina.net) project. We are now using other protocols that allow us to use existing server software (see [this post](https://inqlab.net/2021-11-12-openengiadina-from-activitypub-to-xmpp.html) and [this thread](https://socialhub.activitypub.rocks/t/openengiadina-from-activitypub-to-xmpp/2106)).

CPub is the result of much research and development into how generic and decentralized data models can be used over the ActivityPub protocol. Projects or individuals who are interested in continuing the development of the ideas within the ActivityPub protocol may be interested in using CPub as a starting point and we would be very happy to support you in such an endeavor. Please feel free to get in contact.

The work towards this release has been done by @rustra who is currently being detained as a political prisoner in Belarus. His detainment is not only a loss to his family and friends but to the entire ActivityPub and Free Software community. Resist political repression and support the victims! Please consider donating to the [Anarchist Black Cross Belarus](https://abc-belarus.org/?page_id=8661&lang=en). @rustra can currently only receive letters in Russian. If you write Russian and would like to send him a letter that would be great. Please contact me for his address.

Resist and stay safe!
